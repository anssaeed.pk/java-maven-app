def cleanAPP(){
    sh 'mvn clean package'
}
def buildAPP(){
    sh 'mvn package'
}
def buildImage(){
    withCredentials([
        usernamePassword(credentialsId:'dockerhub-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')
    ]){
        sh "docker build -t anssaeed/my-repo:${IMAGE_NAME} ."
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh "docker push anssaeed/my-repo:${IMAGE_NAME}"
    }

}

def incrementVersion(){
      sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
                        def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                        def version =matcher[0][1]
                        env.IMAGE_NAME="$version-$BUILD_NUMBER"
}

def commitVersion(){
      withCredentials([
        usernamePassword(credentialsId:'eb7ded11-68c4-4f9e-9f2f-dd16e659433c', usernameVariable: 'USER', passwordVariable: 'PASS')
    ]){
        // sh 'git config user.email "jenkins@example.com"'
        // sh 'git config user.name "jenkins"'

        // sh 'git status'
        // sh 'git config --list'
        // sh 'git branch'

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/anssaeed.pk/java-maven-app.git"

        sh 'git add .'
        sh 'git commit -m "[ci skip] : version bump"'
        sh 'git push origin HEAD:master'
    }
}


return this